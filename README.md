## Create and run container in the background
~~~
sudo docker-compose up -d
~~~
## Stop a container in the background
~~~
docker-compose -f <yml file name> down
~~~
#### Read logs of a container in the background
~~~
docker-compose -f <yml file name> logs
~~~

